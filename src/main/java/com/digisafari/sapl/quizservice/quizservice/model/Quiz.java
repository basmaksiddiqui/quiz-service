package com.digisafari.sapl.quizservice.quizservice.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "quizzes")
public class Quiz {
	
	@Id
	private String id;
	private String quizId;
	private String title;
	private String questionType;
	private int marks;
	private int negativeMarks;
	private String taxonomyLevel;
	private float createdOn;
	private float updatedOn;
	private Options options;
	private String incorrectAnswerExplanation;
	
	public Quiz() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuizId() {
		return quizId;
	}

	public void setQuizId(String quizId) {
		this.quizId = quizId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public int getMarks() {
		return marks;
	}

	public void setMarks(int marks) {
		this.marks = marks;
	}

	public int getNegativeMarks() {
		return negativeMarks;
	}

	public void setNegativeMarks(int negativeMarks) {
		this.negativeMarks = negativeMarks;
	}

	public String getTaxonomyLevel() {
		return taxonomyLevel;
	}

	public void setTaxonomyLevel(String taxonomyLevel) {
		this.taxonomyLevel = taxonomyLevel;
	}

	public float getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(float createdOn) {
		this.createdOn = createdOn;
	}

	public float getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(float updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Options getOptions() {
		return options;
	}

	public void setOptions(Options options) {
		this.options = options;
	}

	public String getIncorrectAnswerExplanation() {
		return incorrectAnswerExplanation;
	}

	public void setIncorrectAnswerExplanation(String incorrectAnswerExplanation) {
		this.incorrectAnswerExplanation = incorrectAnswerExplanation;
	}

	@Override
	public String toString() {
		return "Quiz [id=" + id + ", quizId=" + quizId + ", title=" + title + ", questionType=" + questionType
				+ ", marks=" + marks + ", negativeMarks=" + negativeMarks + ", taxonomyLevel=" + taxonomyLevel
				+ ", createdOn=" + createdOn + ", updatedOn=" + updatedOn + ", options=" + options
				+ ", incorrectAnswerExplanation=" + incorrectAnswerExplanation + "]";
	}
	
	
	
}
