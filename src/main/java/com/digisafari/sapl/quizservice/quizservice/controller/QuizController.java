package com.digisafari.sapl.quizservice.quizservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.digisafari.sapl.quizservice.quizservice.exceptions.QuestionAlreadyExistsException;
import com.digisafari.sapl.quizservice.quizservice.exceptions.QuestionNotFoundException;
import com.digisafari.sapl.quizservice.quizservice.model.Quiz;
import com.digisafari.sapl.quizservice.quizservice.service.IQuizService;


@RestController
@RequestMapping("api/v1")
public class QuizController {
	
	ResponseEntity<?> responseEntity;
	
	@Autowired
	IQuizService quizService;
	
	
	@PostMapping("/quiz")
	public ResponseEntity<?> addQuestion(@RequestBody Quiz quiz) throws QuestionAlreadyExistsException{
		Quiz createdQuestion = null;
		try {
			createdQuestion = quizService.addQuestion(quiz);
			if(createdQuestion != null)
			responseEntity = new ResponseEntity<>(createdQuestion, HttpStatus.CREATED);
			else
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again", HttpStatus.INTERNAL_SERVER_ERROR);
		} catch(QuestionAlreadyExistsException questionAlreadyExistsException) {
			throw questionAlreadyExistsException;
		} catch(Exception e) {
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@GetMapping("/quiz")
	public ResponseEntity<?> getAllQuestions(){
		try {
			List<Quiz> questionsList = quizService.getAllQuestions();
			responseEntity = new ResponseEntity<>(questionsList, HttpStatus.OK);
		} catch (Exception e) {
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again", HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		return responseEntity;
	}
	
	@GetMapping("/quiz/{questionId}")
	public ResponseEntity<?> getQuestionById(@PathVariable("questionId") String id) throws QuestionNotFoundException{
		try {
			Quiz getQuestionById = quizService.getQuestionById(id);
			responseEntity = new ResponseEntity<>(getQuestionById, HttpStatus.OK);
		} catch(QuestionNotFoundException questionNotFoundException) {
			throw questionNotFoundException;
		} catch(Exception e) {
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@PutMapping("/quiz/{questionId}")
	public ResponseEntity<?> updateQuestion(@PathVariable("questionId") Quiz quiz) throws QuestionNotFoundException{
		Quiz updatedQuestion = null;
		try {
			updatedQuestion = quizService.updateQuestion(quiz);
			if(updatedQuestion != null)
			responseEntity = new ResponseEntity<>(updatedQuestion, HttpStatus.OK);
			else
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again", HttpStatus.INTERNAL_SERVER_ERROR);
		} catch(QuestionNotFoundException questionNotFoundException) {
			throw questionNotFoundException;
		} catch(Exception e) {
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@DeleteMapping("/quiz/{questionId}")
	public ResponseEntity<?> deleteQuestion(@PathVariable("questionId") String id) throws QuestionNotFoundException{
		boolean status = false;
		try {
			status = quizService.deleteQuestion(id);
			if(status == true)
			responseEntity = new ResponseEntity<>("Course successfully deleted", HttpStatus.OK);
			else
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again", HttpStatus.INTERNAL_SERVER_ERROR);
		} catch(QuestionNotFoundException questionNotFoundException) {
			throw questionNotFoundException;
		} catch(Exception e) {
			responseEntity = new ResponseEntity<>("Some internal error occured. Please try again", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
}

