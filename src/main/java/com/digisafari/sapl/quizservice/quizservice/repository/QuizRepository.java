package com.digisafari.sapl.quizservice.quizservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.digisafari.sapl.quizservice.quizservice.model.Quiz;

@Repository
public interface QuizRepository extends MongoRepository<Quiz, String> {

}
