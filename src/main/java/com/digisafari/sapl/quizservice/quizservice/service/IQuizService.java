package com.digisafari.sapl.quizservice.quizservice.service;

import java.util.List;

import com.digisafari.sapl.quizservice.quizservice.exceptions.QuestionAlreadyExistsException;
import com.digisafari.sapl.quizservice.quizservice.exceptions.QuestionNotFoundException;
import com.digisafari.sapl.quizservice.quizservice.model.Quiz;


public interface IQuizService {


	// business method
	public Quiz addQuestion(Quiz quiz) throws QuestionAlreadyExistsException;
	public List<Quiz> getAllQuestions();
	public Quiz getQuestionById(String id) throws QuestionNotFoundException;
	public Quiz updateQuestion(Quiz quiz) throws QuestionNotFoundException;
	public boolean deleteQuestion(String id) throws QuestionNotFoundException;
}
