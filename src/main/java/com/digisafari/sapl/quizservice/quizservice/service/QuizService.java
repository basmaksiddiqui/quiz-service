package com.digisafari.sapl.quizservice.quizservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.digisafari.sapl.quizservice.quizservice.exceptions.QuestionAlreadyExistsException;
import com.digisafari.sapl.quizservice.quizservice.exceptions.QuestionNotFoundException;
import com.digisafari.sapl.quizservice.quizservice.model.Quiz;
import com.digisafari.sapl.quizservice.quizservice.repository.QuizRepository;

@Service
public class QuizService implements IQuizService {
	
	@Autowired
	QuizRepository quizRepository;
	

	@Override
	public Quiz addQuestion(Quiz quiz) throws QuestionAlreadyExistsException {
		Quiz createdQuestion = null;
		try {
			Optional<Quiz> optionalQuestion = quizRepository.findById(quiz.getId());
			if(optionalQuestion.isPresent()) {
				throw new QuestionAlreadyExistsException();
			} else {
				createdQuestion = quizRepository.save(quiz);
			}
		} catch (QuestionAlreadyExistsException e) {
			throw e;
		}
		return createdQuestion;
	}

	@Override
	public List<Quiz> getAllQuestions() {

		return quizRepository.findAll();
	}

	@Override
	public Quiz getQuestionById(String id) throws QuestionNotFoundException {
		Quiz questionById = null;
		try {
			Optional<Quiz> optionalQuestion = quizRepository.findById(id);
			if(optionalQuestion.isPresent()) {
				questionById = optionalQuestion.get();
			} else {
				throw new QuestionNotFoundException();
			}	
		}	catch (QuestionNotFoundException e) {
			throw e;
		}
		return questionById; 
	}

	@Override
	public Quiz updateQuestion(Quiz quiz) throws QuestionNotFoundException {
		
		Quiz updatedQuestion = null;
		try {
			Optional<Quiz> optionalQuestion = quizRepository.findById(quiz.getId());
			if(optionalQuestion.isPresent()) {
				updatedQuestion = quizRepository.save(quiz);
			} else {
				throw new QuestionNotFoundException();
			}	
		}	catch (QuestionNotFoundException e) {
			throw e;
		}
		return updatedQuestion;
	}

	@Override
	public boolean deleteQuestion(String id) throws QuestionNotFoundException {

		boolean status = false;
		try {
			Optional<Quiz> optionalQuestion = quizRepository.findById(id);
			if(optionalQuestion.isPresent()) {
				 quizRepository.delete(optionalQuestion.get());
				 status = true;
			} else {
				throw new QuestionNotFoundException();
			}	
		}	catch (QuestionNotFoundException e) {
			throw e;
			
		}
		return status;
	} 

}


