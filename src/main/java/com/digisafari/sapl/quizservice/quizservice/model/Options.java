package com.digisafari.sapl.quizservice.quizservice.model;

public class Options {
	private int id;
	private String value;
	private boolean isCorrect;
	private String description;
	
	public Options() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isCorrect() {
		return isCorrect;
	}

	public void setCorrect(boolean isCorrect) {
		this.isCorrect = isCorrect;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Options [id=" + id + ", value=" + value + ", isCorrect=" + isCorrect + ", description=" + description
				+ "]";
	}
	
}
